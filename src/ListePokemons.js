import React, {useEffect, useState} from "react";
import Pokemon from "./Pokemon.js"
import {serveur} from "./constantes";

export default function ListePokemons() {
  const [habitats, setHabitats] = useState([]);
  const [species, setSpecies] = useState([]);
  const [poketypes, setPoketypes] = useState([]);
  const [pokemons, setPokemons] = useState([]);
  const [habitatFilter, setHabitatFilter] = useState("");
  const [speciesFilter, setSpeciesFilter] = useState("")
  const [poketypeFilter, setPoketypeFilter] = useState("");

  function handleChangeHabitat(event) {
    setHabitatFilter(event.target.value);
  }

  function handleChangeSpecies(event) {
    setSpeciesFilter(event.target.value);
  }

  function handleChangePoketype(event) {
    setPoketypeFilter(event.target.value);
  }

  function filteredPokemons() {
    let p = pokemons;

    if (habitatFilter !== "") {
      p = p.filter((p) => p.Habitat.HabitatId === parseInt(habitatFilter))
    }
    if (speciesFilter !== "") {
      p = p.filter((p) => p.Species.SpeciesId === parseInt(speciesFilter))
    }
    if (poketypeFilter !== "") {
      p = p.filter((p) => p.PokeTypes.filter((t) => t.PokeTypeId === parseInt(poketypeFilter)).length > 0)
    }
    return p;
  }

  async function componentDidMount() {
    // obtenir les poketypes
    let urlPoketypes = `${serveur}/api/poketypes`;
    let resultatPoketypes = await fetch(urlPoketypes);
    if (resultatPoketypes.ok) {
      let data = await resultatPoketypes.json();
      setPoketypes(data);
    } else {
      console.log("une erreur s'est produite lors de l'appel à /api/poketypes");
    }

    // obtenir les habitats
    let urlHabitats = `${serveur}/api/habitats`;
    let resultatHabitats = await fetch(urlHabitats);
    if (resultatHabitats.ok) {
      let data = await resultatHabitats.json();
      setHabitats(data)
    } else {
      console.log("une erreur s'est produite lors de l'appel à /api/habitats");
    }

    // obtenir les species
    let urlSpecies = `${serveur}/api/species`;
    let resultatSpecies = await fetch(urlSpecies);
    if (resultatSpecies.ok) {
      let data = await resultatSpecies.json();
      setSpecies(data)
    } else {
      console.log("une erreur s'est produite lors de l'appel à /api/species");
    }

    // obtenir les pokemons
    let urlPokemons = `${serveur}/api/pokemons`;
    let resultatPokemons = await fetch(urlPokemons);
    if (resultatPokemons.ok) {
      let data = await resultatPokemons.json();
      setPokemons(data)
    } else {
      console.log("une erreur s'est produite lors de l'appel à /api/pokemons");
    }
  }

  useEffect(() => {
    console.log("useEffect was called");
    componentDidMount();
  }, [habitatFilter, speciesFilter, poketypeFilter]);

  return (
    <div className="section">
      <div className="columns is-centered">
        <div className="field is-horizontal" style={{paddingLeft: "20px"}}>
          <div className="field-label is-normal">
            <label className="label" htmlFor="habitatFilter">Habitat</label>
          </div>
          <div className="field-body">
            <div className="field">
              <div className="control" style={{minWidth: "200px"}}>
                <div className="select is-fullwidth">
                  <select onChange={handleChangeHabitat}>
                    <option/>
                    {
                      habitats.map((habitat) => {
                        return <option
                          value={habitat.HabitatId}
                          key={habitat.HabitatId}>
                          {habitat.Name}
                        </option>
                      })
                    }
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="field is-horizontal" style={{paddingLeft: "20px"}}>
          <div className="field-label is-normal">
            <label className="label" htmlFor="speciesFilter">Species</label>
          </div>
          <div className="field-body">
            <div className="field">
              <div className="control" style={{minWidth: "200px"}}>
                <div className="select is-fullwidth">
                  <select onChange={handleChangeSpecies}>
                    <option/>
                    {
                      species.map((species) => {
                        return <option
                          key={species.SpeciesId} value={species.SpeciesId}>
                          {species.Name}
                        </option>
                      })
                    }
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="field is-horizontal" style={{paddingLeft: "20px"}}>
          <div className="field-label is-normal">
            <label className="label" htmlFor="poketypeFilter">Poketypes</label>
          </div>
          <div className="field-body">
            <div className="field">
              <div className="control" style={{minWidth: "200px"}}>
                <div className="select is-fullwidth">
                  <select onChange={handleChangePoketype}>
                    <option/>
                    {
                      poketypes.map((poketype) => {
                        return <option
                          key={poketype.PokeTypeId} value={poketype.PokeTypeId}>
                          {poketype.Name}
                        </option>
                      })
                    }
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="row columns is-multiline">
        {
          filteredPokemons().map((pokemon) => {
            return <Pokemon pokemon={pokemon} key={pokemon.PokemonId}/>;
          })
        }
      </div>
    </div>
  );
}
