import ListePokemons from "./ListePokemons";
import {BrowserRouter, Route, Routes} from "react-router-dom";
import DetailsPokemon from "./DetailsPokemon";
import Login from "./Login";
import Menu from "./Menu";
import Favoris from "./Favoris";
import { createContext, useState } from "react";


export const tokenContext = createContext();


export default function App() {
  const [token, setToken] = useState("");
  const objetContext = { token, setToken };


  return (
    <>
    <tokenContext.Provider value={objetContext}>   
    <BrowserRouter>
      <div className="container">
      <Menu />
        <Routes>
          <Route path="/" element={<ListePokemons/>}/>
          <Route path="/detailspokemon/:id" element={<DetailsPokemon/>}/>
          <Route path="/login" element={<Login/>}/>
          { token !== "" &&
          <Route path="/favoris" element={<Favoris/>}/>
          }
        </Routes>
      </div>
    </BrowserRouter>
    </tokenContext.Provider>

  </>
  );
}
