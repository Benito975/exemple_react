import React from "react";
import {Link} from 'react-router-dom';


export default function Pokemon(props) {
  return (
    <div className="column is-3">
      <Link to={'/detailspokemon/'+props.pokemon.PokemonId}>
      <div className="card has-text-black" style={{backgroundColor: props.pokemon.Color}}>
        <div className="card-image">
          <figure className="image is-square">
            <img alt={props.pokemon.Name} src={props.pokemon.ImgURL}/>
          </figure>
        </div>
        <div className="card-content">
          <div className="content">
            <p className="title is-3 has-text-centered">{props.pokemon.Name}</p>
            <div className="mb-0" style={{color: "black"}}>
              <span className="has-text-weight-bold">Species: </span>
              <span>{props.pokemon.Species.Name}</span>
            </div>
            <div className="mb-0">
              <span className="has-text-weight-bold">Habitat: </span>
              <span>{props.pokemon.Habitat.Name}</span>
            </div>
            <div className="mb-0">
              <span>
                <span className="has-text-weight-bold">PokeTypes: </span>
                {
                  props.writers.map((poketype) => {
                    return poketype.Name
                  }).join(" / ")
                }
              </span>
            </div>
          </div>
        </div>
      </div>
      </Link>
    </div>
      )
}
