import React, {useEffect, useState, useContext} from "react";
import {serveur} from "./constantes";
import { tokenContext } from "./App";
import Pokemon from "./Pokemon.js"



export default function Favoris() {
    const [pokemons, setPokemons] = useState([]);
    const token = useContext(tokenContext);



    useEffect(() => {
        async function componentDidMount() {
            const srvURL = `${serveur}/api/favorites`;
            const bearerToken = `bearer ${token.token}`;
            const response = await fetch(srvURL, {
                method: 'GET',
                headers: {
                Authorization: bearerToken,
                },
            });
            if (response.ok) {
                const data = await response.json();
                setPokemons(data);
            }
        }
        componentDidMount()
    }, [token])

    return (
        <>
         <div className="section">
            <div className="columns is-centered">
            <div className='subtitle is-3'>
                Favorites
            </div>
            </div>
            {
                pokemons &&
                <div className="row columns is-multiline is-centered">
                {
                pokemons.map((pokemon) => {
                return <Pokemon pokemon={pokemon} key={pokemon.id}/>
                })
                }
                </div>
            }
        </div>
        </>
    )
}