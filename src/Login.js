import { useContext, useState } from "react";
import { tokenContext } from "./App";
import {serveur} from "./constantes";
import {useNavigate} from "react-router-dom";


export default function Login() {
  const navigate = useNavigate();
  const [login, setLogin] = useState("");
  const [passwd, setPasswd] = useState("");
  const token = useContext(tokenContext);

  function handleChangeLogin(event) {
    setLogin(event.target.value);
  }

  function handleChangePasswd(event) {
    setPasswd(event.target.value);
  }


async function obtenirJeton() {
  const bodyContent = `grant_type=password&username=${login}&password=${passwd}`;

  const response = await fetch(`${serveur}/token`, {
    method: 'POST',
    body: bodyContent,
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
  });

  if (response.ok) {
    const data = await response.json();
    token.setToken(data.access_token);
    navigate("/favoris");
  } else {
    console.error(response.statusText);
  }
}

function annuler() {
  setLogin('');
  setPasswd('');
  token.setToken('');
  navigate("/");
}

    return (
        <div className="container">
        <div className="section">
            <div className="columns is-centered">
                <div className='column is-3'>
                    <div className="field">
                        <label className="label">Email</label>
                        <p className="control has-icons-left has-icons-right">
                        <input className="input" type="email" onChange={handleChangeLogin}/>
                        <span className="icon is-small is-left">
                            <i className="fas fa-envelope"></i>
                        </span>
                        </p>
                    </div>
                </div>
            </div>
            <div className="columns is-centered">
                <div className='column is-3'>
                  <div className="field">
                    <label className="label">Password</label>
                    <p className="control has-icons-left">
                      <input className="input" type="password" onChange={handleChangePasswd}/>
                      <span className="icon is-small is-left">
                        <i className="fas fa-lock"></i>
                      </span>
                    </p>
                  </div>
                </div>
            </div>
            <div className="columns is-centered">
                <div className="column is-3">
                  <div className="field">
                    <p className="control">
                      <button onClick={obtenirJeton} className="button is-success">
                        Connexion
                      </button>
                      <button onClick={annuler} className="button is-danger">
                        Annuler
                      </button>
                    </p>
                  </div>
                </div>
            </div>
        </div>
   </div>
    )
}
