import {Link} from 'react-router-dom';
import { tokenContext } from "./App";
import { useContext} from "react";



export default function Menu() {
    const token = useContext(tokenContext);

    function deco() {
        token.setToken('');
      }

    return (
    <div className="column">
    <nav className="navbar" role="navigation" aria-label="main navigation">
    <div id="navbarBasicExample" className="navbar-menu">
            <div className="navbar-start">
                <Link className="navbar-item has-text-grey" to="/">
                Exercice 07
                </Link>
                {
                    token.token !== "" &&
                    <Link className="navbar-item has-text-grey" to="/favoris">
                    Favorites
                    </Link>
                }
            </div>

        <div className="navbar-end">
        <div className="navbar-item">
            <div className="buttons">
            {
                token.token === "" &&
                <Link className="button is-light" to="/login">
                    Login
                </Link>
            }
            {
                token.token !== "" &&
                <Link to="/">
                    <button className="button is-danger" onClick={deco}>
                        Logout
                    </button>
                </Link>
            }
            </div>
        </div>
        </div>
    </div>
    </nav>
    </div>
    )
}