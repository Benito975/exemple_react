import {serveur} from "./constantes";
import React, {useEffect, useState, useContext} from "react";
import { useParams } from "react-router";
import Pokemon from "./Pokemon";
import { tokenContext } from "./App";



export default function DetailsPokemon() {
    const params = useParams();
    const [IsFavorite, setIsFavorite] = useState("");
    const [pokemons, setPokemons] = useState(null);
    const token = useContext(tokenContext);


    useEffect(() => {
        console.log("useEffect was called");
        async function componentDidMount() {
            // obtenir le details
            let urlDetails = `${serveur}/api/pokemon?id=${params.id}`;
            console.log(params.id)
            let resultatDetails = await fetch(urlDetails);
            if (resultatDetails.ok) {
            let data = await resultatDetails.json();
            setPokemons(data);
            } else {
            console.log("une erreur s'est produite lors de l'appel à /api/pokemon?id=4");
            }
        }
        componentDidMount();
        
        async function getFavorites(pokemonid) {
            const srvURL = `${serveur}/api/favorites?PokemonId=${pokemonid}`;
            const bearerToken = `bearer ${token.token}`;
            const response = await fetch(srvURL, {
              method: 'GET',
              headers: {
                Authorization: bearerToken,
              },
            });
            if (response.ok) {
                const data = await response.json();
                setIsFavorite(data.IsFavorite);
            }
          }
        if (token.token !== "") {
            getFavorites(params.id)
        }
      }, [params.id, token.token]);


      async function ajouterFavoris() {
        const srvURL = `${serveur}/api/favorites?PokemonId=${params.id}`;
        const bearerToken = `bearer ${token.token}`;
        const response = await fetch(srvURL, {
          method: 'POST',
          headers: {
            Authorization: bearerToken,
          },
        });
        if (response.ok) {
          setIsFavorite(true)
          console.log("add");
        }
      }
  
      async function supprFavoris() {
        const srvURL = `${serveur}/api/favorites?PokemonId=${params.id}`;
        const bearerToken = `bearer ${token.token}`;
        const response = await fetch(srvURL, {
          method: 'DELETE',
          headers: {
            Authorization: bearerToken,
          },
        });
        if (response.ok) {
          setIsFavorite(false)
          console.log("suppr");
        }
      }

    return (
        <div>
            {pokemons !== null &&
        
        <div className="content">
            <div className="has-text-centered">
            <div>
            { token.token !== '' &&
            <>
                { IsFavorite === false &&
                <button className="button" onClick={ajouterFavoris}>Ajouter au favoris</button>
                }
                { IsFavorite === true &&
                <button className="button" onClick={supprFavoris}>Supprimer des favoris</button>
                }
            </>
            }
            </div>
                <img alt={pokemons.Name} src={pokemons.ImgURL}/>
                <div>Name: {pokemons.Name}</div>
                <div>HP: {pokemons.HP}</div>
                <div>Attack: {pokemons.Attack}</div>
                <div>Defense: {pokemons.Defense}</div>
                <div>Height: {pokemons.Height}</div>
                <div>SpecialAttack: {pokemons.Attack}</div>
                <div>SpecialDefense: {pokemons.Defense}</div>
                <div>Speed: {pokemons.Speed}</div>
                <div>Weight: {pokemons.Weight}</div>
                <div>
                    <audio controls>
                        <source src={pokemons.CryURL} type="audio/ogg"/>
                    </audio>
                </div>
                <div>Habitat: {pokemons.Habitat.Name}</div>
                <div>Species: {pokemons.Species.Name}</div>
                <div>Poketypes: {pokemons.PokeTypes.map((p)=> p.Name+" ")}</div>

                {
                    pokemons.Evolution &&
                    <div className="row columns is-multiline is-centered">
                    <Pokemon pokemon={pokemons.Evolution} key={pokemons.Evolution.PokemonId}/>

                    {
                    pokemons.Evolution.Evolution &&
                    <Pokemon pokemon={pokemons.Evolution.Evolution} key={pokemons.Evolution.Evolution.PokemonId}/>

                    }
                    </div>
                    
                }
            </div>
            </div>
        }</div>
    )
}